import javafx.scene.input.KeyCode;
import model.Model;

public class InputHandler {

    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }
    public void onKeyPressed(KeyCode key) {

        if (key == KeyCode.W) {
            model.getPlayer().setMoveUp(true);
        }
        if (key == KeyCode.S) {
            model.getPlayer().setMoveDown(true);
        }
        if (key == KeyCode.A) {
            model.getPlayer().setMoveLeft(true);
        }
        if (key == KeyCode.D) {
            model.getPlayer().setMoveRight(true);
        }

    }
    public void onKeyReleased(KeyCode key) {
        if (key == KeyCode.W) {
            model.getPlayer().setMoveUp(false);
        }
        if (key == KeyCode.S) {
            model.getPlayer().setMoveDown(false);
        }
        if (key == KeyCode.A) {
            model.getPlayer().setMoveLeft(false);
        }
        if (key == KeyCode.D) {
            model.getPlayer().setMoveRight(false);
        }
    }

    }

