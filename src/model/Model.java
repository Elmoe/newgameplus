package model;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Model {
    public static final int WIDTH = 1280;
    public static final int HEIGHT = 720;
    private List<Bee> bees = new LinkedList<>();
    private List<Coin> coins = new LinkedList<>();
    private int points = 0;
    private int life = 3;
    private int carCounter = 0;
    private long time;
    private int modelState;

    private Player player;


    public List<Bee> getBees() {
        return bees;
    }

    public List<Coin> getCoins() {
        return coins;
    }

    public Player getPlayer() {
        return player;
    }

    public int getLife() {
        return life;
    }

    public int getPoints() {
        return points;
    }

    public Model() {
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            this.bees.add(new Bee(0, random.nextInt(620), 0.1f + random.nextFloat() * 0.2f));
        }

        this.coins.add(new Coin(random.nextInt(800 + 50), random.nextInt(400 + 50)));
        this.player = new Player(640, 670);
    }
    public long getTime() {
        return time;
    }

    public void timer(long elapsedTime) {
        if (time > 1500) {
            this.time = 0;
        }
        time += elapsedTime;
        if (time < 500) {
            this.modelState = 1;
        } else if (time < 1000) {
            this.modelState = 2;
        } else if (time < 1500) {
            this.modelState = 3;
        }
    }

    public int getModelState() {
        return modelState;
    }

    public void update(long elapsedTime) {
        for (Bee bee : bees) {
            bee.update(elapsedTime);
        }
        player.update(elapsedTime);
        // ...
        for (Bee bee : bees) {
            // falls bee und player kollidieren
            // dann player zurück zum start (200, 500)
            int dx = Math.abs(player.getX() - bee.getX());
            int dy = Math.abs(player.getY() - bee.getY());
            int w = player.getW() / 2 + bee.getW() / 2;
            int h = player.getH() / 2 + bee.getH() / 2;

            if (dx <= w && dy <= h) {
                // kollision!
                player.moveTo(640, 700);
                life -= 1;
                if (life < 0) {
                    points = 0;
                    life = 3;
                    //TODO remove last carcounter bees
                    for (int i = 1; i <= carCounter; i++) {
                        bees.remove(bees.size() - 1);
                    }
                    carCounter = 0;
                }
            }
        }

        for (Coin coin : coins) {
            int dx = Math.abs(player.getX() - coin.getX());
            int dy = Math.abs(player.getY() - coin.getY());
            int w = player.getW() / 2 + coin.getW() / 2;
            int h = player.getH() / 2 + coin.getH() / 2;

            if (dx <= w && dy <= h) {
                Random random = new Random();
                coins.remove(coin);
                points++;
                this.coins.add(new Coin(random.nextInt(800 + 50), random.nextInt(400 + 50)));
                if (points % 3 == 0) {
                    if (player.getX() > 640) {
                        this.bees.add(new Bee(0, random.nextInt(620), 0.1f + random.nextFloat() * 0.2f + points * 0.01f));
                        carCounter++;
                    } else {
                        this.bees.add(new Bee(1280, random.nextInt(620), 0.1f + random.nextFloat() * 0.2f + points * 0.01f));
                        carCounter++;
                    }
                    if (points % 10 == 0) {
                        life += 1;
                    }
                }

            }
        }

    }
}
