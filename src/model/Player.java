package model;

public class Player {
    private int x;
    private int y;

    private boolean moveUp;
    private boolean moveDown;
    private boolean moveRight;
    private boolean moveLeft;
    private boolean stationary;

    private int speed = 4;


    public Player(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setMoveUp(boolean moveUp) {
        this.moveUp = moveUp;
    }

    public void setMoveDown(boolean moveDown) {
        this.moveDown = moveDown;
    }

    public void setMoveRight(boolean moveRight) {
        this.moveRight = moveRight;
    }

    public void setMoveLeft(boolean moveLeft) {
        this.moveLeft = moveLeft;
    }

    public void update(long elapsedTime) {
        if (moveUp && !moveDown) {
            this.y -= speed;
        }
        if (moveDown && !moveUp) {
            this.y += speed;
        }
        if (moveRight && !moveLeft) {
            this.x += speed;
        }
        if (moveLeft && !moveRight) {
            this.x -= speed;
        }

    }

    public void moveTo(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getW() {
        return 72;
    }

    public int getH() {
        return 48;
    }

    public int getWU() {
        return 48;
    }

    public int getHU() {
        return 72;
    }

    public boolean isMoveUp() {
        return moveUp;
    }

    public boolean isMoveDown() {
        return moveDown;
    }

    public boolean isMoveRight() {
        return moveRight;
    }

    public boolean isMoveLeft() {
        return moveLeft;
    }

    public boolean isStationary() {
        stationary = (!this.isMoveDown() && !this.isMoveUp() && !this.isMoveRight() && !this.isMoveLeft());
        return stationary;
    }

}
