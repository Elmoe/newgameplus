package model;

public class Bee {
    private int x;
    private int y;
    private float speedX;

    public Bee(int x, int y, float speedX) {
        this.x = x;
        this.y = y;
        this.speedX = speedX;
    }

    public void update(long elapsedTime) {
        this.x = Math.round(this.x + elapsedTime * this.speedX);

        if (this.x > Model.WIDTH && this.speedX > 0 || this.x < 0 && this.speedX < 0) {
            this.speedX = -1 * this.speedX;
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return 34;
    }

    public int getW() {
        return 32;
    }

    public float getSpeedX() {
        return speedX;
    }
}
