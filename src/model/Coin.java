package model;

public class Coin {


    private int x;
    private int y;
    private int points;

    public Coin(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public int getH() {
        return 31;
    }
    public int getW() {
        return 34;
    }

}
