import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;
import model.Model;


public class Main extends Application {

    private Timer timer;

    @Override
    public void start(Stage stage) throws Exception {
        Canvas canvas = new Canvas(Model.WIDTH, Model.HEIGHT);
        Group group = new Group();
        group.getChildren().add(canvas);
        Scene scene = new Scene(group);
        stage.setScene(scene);
        stage.show();

        GraphicsContext gc = canvas.getGraphicsContext2D();
        Model model = new Model();
        Graphics graphics = new Graphics(model, gc);
        timer = new Timer(model, graphics);
        timer.start();

        InputHandler inputHandler = new InputHandler(model);

        scene.setOnKeyPressed(event -> {
            inputHandler.onKeyPressed(event.getCode());
        });

        scene.setOnKeyReleased(event ->
                inputHandler.onKeyReleased(event.getCode()));
    }
    @Override
    public void stop() throws Exception {
        timer.stop();
        super.stop();
    }
}
