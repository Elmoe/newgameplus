import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.Bee;
import model.Coin;
import model.Model;
import model.Player;


public class Graphics {
    private Model model;
    private GraphicsContext gc;

    Image honey = new Image("honey.png");

    Image beeLeft1 = new Image("beeLeft1.png");
    Image beeLeft2 = new Image("beeLeft2.png");
    Image beeLeft3 = new Image("beeLeft3.png");

    Image beeRight1 = new Image("beeRight1.png");
    Image beeRight2 = new Image("beeRight2.png");
    Image beeRight3 = new Image("beeRight3.png");

    Image bearRight1 = new Image("bearRight1.png");
    Image bearRight2 = new Image("bearRight2.png");
    Image bearRight3 = new Image("bearRight3.png");

    Image bearLeft1 = new Image("bearLeft1.png");
    Image bearLeft2 = new Image("bearLeft2.png");
    Image bearLeft3 = new Image("bearLeft3.png");

    Image bearUp1 = new Image("bearUp1.png");
    Image bearUp2 = new Image("bearUp2.png");
    Image bearUp3 = new Image("bearUp3.png");

    Image bearDown1 = new Image("bearDown1.png");
    Image bearDown2 = new Image("bearDown2.png");
    Image bearDown3 = new Image("bearDown3.png");

    Image meadow = new Image("grass.png");

    Image[] beesLeft = new Image[]{beeLeft1, beeLeft2, beeLeft3};
    Image[] beesRight = new Image[]{beeRight1, beeRight2, beeRight3};

    Image[] bearRight = new Image[]{bearRight1, bearRight2, bearRight3};
    Image[] bearLeft = new Image[]{bearLeft1, bearLeft2, bearLeft3};
    Image[] bearDown = new Image[]{bearDown1, bearDown2, bearDown3};
    Image[] bearUp = new Image[]{bearUp1, bearUp2, bearUp3};



    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    public void draw() {
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

        gc.drawImage(meadow, 0, 0, Model.WIDTH, Model.HEIGHT);

        for (Bee bee : this.model.getBees()) {
            if (bee.getSpeedX() > 0) {
                gc.drawImage(beesRight[model.getModelState() - 1],
                        bee.getX() - bee.getW() / 2,
                        bee.getY() - bee.getH() / 2,
                        bee.getW(),
                        bee.getH()
                );
            }
            if (bee.getSpeedX() < 0) {
                gc.drawImage(beesLeft[model.getModelState() - 1],
                        bee.getX() - bee.getW() / 2,
                        bee.getY() - bee.getH() / 2,
                        bee.getW(),
                        bee.getH()
                );

            }
        }

        Player p = model.getPlayer();
        if (model.getPlayer().isMoveLeft() || (model.getPlayer().isMoveLeft() && (model.getPlayer().isMoveDown() || model.getPlayer().isMoveUp()))) {
            gc.drawImage(bearLeft[model.getModelState() - 1],
                    p.getX() - p.getW() / 2,
                    p.getY() - p.getH() / 2,
                    p.getW(),
                    p.getH()
            );
        }
        if (model.getPlayer().isMoveRight() || (model.getPlayer().isMoveRight() && (model.getPlayer().isMoveDown() || model.getPlayer().isMoveUp()))) {
            gc.drawImage(bearRight[model.getModelState() - 1],
                    p.getX() - p.getW() / 2,
                    p.getY() - p.getH() / 2,
                    p.getW(),
                    p.getH()
            );
        }
        if (model.getPlayer().isMoveUp() && (!model.getPlayer().isMoveLeft() && !model.getPlayer().isMoveRight())) {
            gc.drawImage(bearUp[model.getModelState() - 1],
                    p.getX() - p.getW() / 2,
                    p.getY() - p.getH() / 2,
                    p.getWU(),
                    p.getHU()
            );
        }
        if (model.getPlayer().isMoveDown() && (!model.getPlayer().isMoveLeft() && !model.getPlayer().isMoveRight())) {
            gc.drawImage(bearDown[model.getModelState() - 1],
                    p.getX() - p.getW() / 2,
                    p.getY() - p.getH() / 2,
                    p.getWU(),
                    p.getHU()
            );
        }
        if (model.getPlayer().isStationary()) {
            gc.drawImage(bearUp[model.getModelState() - 1],
                    p.getX() - p.getW() / 2,
                    p.getY() - p.getH() / 2,
                    p.getWU(),
                    p.getHU()
            );
        }

        for (Coin c : this.model.getCoins()) {
            gc.drawImage(honey, c.getX(), c.getY(), c.getW(), c.getH());
        }

        gc.setFill(Color.WHITE);
        gc.setFont(Font.font(30));

        gc.fillText("Punkte: " + model.getPoints(), 0, 710);
        gc.fillText("Leben: " + model.getLife(), 150, 710);
        gc.strokeText("Punkte: " + model.getPoints(), 0, 710);
        gc.strokeText("Leben: " + model.getLife(), 150, 710);
    }
        }


